#!/usr/bin/env bash

function discoverSystem {
    unameOut="$(uname -s)"
    case "${unameOut}" in
        Linux*)     machine=Linux;;
        Darwin*)    machine=Mac;;
        CYGWIN*)    machine=Cygwin;;
        MINGW*)     machine=MinGw;;
        *)          machine="UNKNOWN:${unameOut}"
    esac
    echo ${machine}
}

function discoverSudo {
    isSudo=$(whereis sudo)
    if [[ ${isSudo} == "sudo:" ]];then
        echo "no"
    else
        echo "yes"
    fi
}
function say() {
     echo "$@" | sed \
             -e "s/\(\(@\(red\|green\|yellow\|blue\|magenta\|cyan\|white\|reset\|b\|u\)\)\+\)[[]\{2\}\(.*\)[]]\{2\}/\1\4@reset/      g" \
             -e "s/@red/$(tput setaf 1)/g"   \
             -e "s/@green/$(tput setaf 2)/g" \
             -e "s/@yellow/$(tput setaf 3)/g"        \
             -e "s/@blue/$(tput setaf 4)/g"  \
             -e "s/@magenta/$(tput setaf 5)/g"       \
             -e "s/@cyan/$(tput setaf 6)/g"  \
             -e "s/@white/$(tput setaf 7)/g" \
             -e "s/@reset/$(tput sgr0)/g"    \
             -e "s/@b/$(tput bold)/g"        \
             -e "s/@u/$(tput sgr 0 1)/g"
}

function setPermissions {
    machine=$(discoverSystem)
    isSudo=$(discoverSudo)
    HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

    if [ ${machine} == "Linux" ];
    then
        if [ ${isSudo} == "yes" ];then
            sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
            sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
        else
            setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
            setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
        fi
    elif [ ${machine} == "Mac" ];
    then
        rm -rf var/logs/* var/cache/*
        if [ ${isSudo} == "yes" ];then
            sudo chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" var var/cache var/logs
            sudo chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" var var/cache var/logs
        else
            chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" var var/cache var/logs
            chmod +a "$(whoami) allow delete,write,append,file_inherit,directory_inherit" var var/cache var/logs
        fi
    fi
}

function askBool {
    say @b@cyan[["${@} (s-skip) [Y/n/s]"]]
    exec < /dev/tty
    read answer
    exec <&-
    if [[ ${answer} == 'Y' || ${answer} == 'y' || ${answer} == 't' || ${answer} == 'T' ]];then
        return 1
    elif [[ ${answer} == 'S' || ${answer} == 's' ]];then
        return 2
    fi

    return 0
}

function generateJwtTokens {
    if [ ! -f "$(pwd)/var/jwt/private.pem" ];
    then
        mkdir var/jwt
        openssl genrsa -out var/jwt/private.pem -aes256 4096
        openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem
    fi
}

function createDatabase {
    bin/console doctrine:database:create
}

function createSchema {
    bin/console doctrine:schema:create
}

function cacheClear {
    bin/console cache:clear --no-warmup
}

function phpqa {
    docker run -i --rm -v $(pwd):/project -w /project jakzal/phpqa:alpine ${@}
}

function phpCsFixer {
     phpqa php-cs-fixer ${1}
}

function installPreCommit {
    if [[ ! -f $(pwd)/.git/hooks/pre-commit ]];then
        isSudo=$(discoverSudo)
        ln -s $(pwd)/pre-commit.sh $(pwd)/.git/hooks/pre-commit
        if [[ isSudo == 1 ]];then
            sudo chmod +x $(pwd)/.git/hooks/pre-commit
        else
            chmod +x $(pwd)/.git/hooks/pre-commit
        fi
    fi
}

function delayedDatabaseCreate {
    set +a
    . .env
    set -a
    MYSQL_HOST=$SYMFONY__DATABASE_HOST:$SYMFONY__DATABASE_PORT
    while ! curl -s $SYMFONY__DATABASE_HOST:$SYMFONY__DATABASE_PORT > /dev/null; do echo "waiting for $MYSQL_HOST ..."; sleep 3; done;
    createSchema
    echo "DATABASE CREATED"
}

function installDocker {
     /bin/bash ./docker/compose.sh build
    /bin/bash ./docker/compose.sh up -d
}

function startDocker {
    /bin/bash ./docker/compose.sh up -d
}

function stopDocker {
    /bin/bash ./docker/compose.sh kill
}

function mailcatcher {
    if [[ ${1} == 'start' ]];then
        docker run --net host --name Mailcatcher -d -p 1080:1080 -p 1025:1025 schickling/mailcatcher
    else
        docker kill Mailcatcher
    fi
}

function setDatabase {
    askBool "Install Database on Docker?"
    if [[ ${?} == 1 ]];then
        installDocker
        delayedDatabaseCreate
    elif [[ ${?} == 0 ]];then
        createDatabase
        createSchema
    fi
}

if [[ ${1} == 'initial_settings' ]];then
    setPermissions
    installPreCommit
    generateJwtTokens
    setDatabase
    cacheClear
fi

if [[ ${1} == 'set_permissions' ]];then
    setPermissions
fi

if [[ ${1} == 'create_database' ]];then
    createDatabase
    createSchema
fi

if [[ ${1} == 'create_migration' ]];then
    bin/console doctrine:migrations:diff
fi

if [[ ${1} == 'migrate' ]];then
    bin/console doctrine:migrations:migrate
fi

if [[ ${1} == 'generate_jwt_tokens' ]];then
    generateJwtTokens
fi

# /bin/bash replace_git_repository http://your-repository.url.git
if [[ ${1} == 'replace_git_repository' ]];then
    if [ -n ${2} ];then
        rm -rf ./.git
        git init
        git remote add origin ${2}
        installPreCommit
    fi
fi

if [[ ${1} == 'cache_clear' ]];then
    cacheClear
fi

if [[ ${1} == 'lint' ]];then
    phpCsFixer 'fix --dry-run --diff'
fi

if [[ ${1} == 'fix' ]];then
    phpCsFixer 'fix'
fi

if [[ ${1} == 'install_pre_commit' ]];then
    installPreCommit
    echo "installed"
fi

if [[ ${1} == 'phpqa' ]];then
    phpqa ${@:2}
fi

if [[ ${1} == 'install_docker' ]];then
    installDocker
fi

if [[ ${1} == 'start' ]];then
    echo "Starting services"
    startDocker
    mailcatcher "start"
fi

if [[ ${1} == 'start' ]];then
    echo "Stopping services"
    stopDocker
    mailcatcher "stop"
fi

if [[ ${1} == 'mailcatcher' ]];then
    mailcatcher "start"
fi

if [[ ${1} == 'rm'  || ${1} == 'remove_docker_database' ]];then
    docker kill docker_mysqldb_1
    docker rm $(docker ps -aq)
    docker volume rm docker_dbdata
fi
