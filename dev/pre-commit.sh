#!/usr/bin/env bash
function phpCsFixer {
    docker run -i --rm -v $(pwd):/project -w /project jakzal/phpqa:alpine php-cs-fixer ${1}
    return ${?}
}

 say() {
     echo "$@" | sed \
             -e "s/\(\(@\(red\|green\|yellow\|blue\|magenta\|cyan\|white\|reset\|b\|u\)\)\+\)[[]\{2\}\(.*\)[]]\{2\}/\1\4@reset/      g" \
             -e "s/@red/$(tput setaf 1)/g"   \
             -e "s/@green/$(tput setaf 2)/g" \
             -e "s/@yellow/$(tput setaf 3)/g"        \
             -e "s/@blue/$(tput setaf 4)/g"  \
             -e "s/@magenta/$(tput setaf 5)/g"       \
             -e "s/@cyan/$(tput setaf 6)/g"  \
             -e "s/@white/$(tput setaf 7)/g" \
             -e "s/@reset/$(tput sgr0)/g"    \
             -e "s/@b/$(tput bold)/g"        \
             -e "s/@u/$(tput sgr 0 1)/g"
  }

say @b@cyan[[Start linting...]]

phpCsFixer 'fix --dry-run --diff'
status=${?}
if [[ ${status} == 0 ]];then
    say @b@green[[Linting OK]]
else
    say @b@red[[Linting ERROR. Fix your code before commit]]
    say @b@cyan[[Fix errors? [Y/n]]]
    exec < /dev/tty
    read flag
    exec <&-
    if [[ ${flag} == 'Y' || ${flag} == 'y' || ${flag} == 't' || ${flag} == 'T' ]];then
        phpCsFixer 'fix'
        say @b@green[[Errors fixed]]
        status=0
    fi
fi

exit ${status}