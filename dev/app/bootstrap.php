<?php

use Dotenv\Dotenv;

require __DIR__.'/autoload.php';
if (file_exists(__DIR__.'../var/bootstrap.php.cache')) {
    if (PHP_VERSION_ID < 70000) {
        include_once __DIR__.'../var/bootstrap.php.cache';
    }
}

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
