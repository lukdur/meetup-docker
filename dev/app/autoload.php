<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;
use Symfony\Component\HttpKernel\Kernel;

/** @var ClassLoader $loader */
$loader = require __DIR__.'/../vendor/autoload.php';

if (Kernel::VERSION_ID < 30200) {
    AnnotationRegistry::registerLoader([$loader, 'loadClass']);
}

return $loader;
