<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use DomainBundle\AbstractKernel;

class AppKernel extends AbstractKernel
{
    public function registerBundles()
    {
        $bundles = array_merge(parent::registerBundles(), [
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new ApiBundle\ApiBundle(),
        ]);

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return '/var/cache/app/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return '/var/log/app';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
