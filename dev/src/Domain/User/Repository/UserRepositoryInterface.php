<?php

namespace Domain\User\Repository;

use Btn\Domain\Exception\EntityNotFoundException;
use Domain\User\Model\User;
use Btn\Domain\ValueObject\Id;
use Symfony\Component\Security\Core\User\UserProviderInterface;

interface UserRepositoryInterface extends UserProviderInterface
{
    /**
     * @param User $user
     */
    public function save(User $user);

    /**
     * @param User $item
     */
    public function remove(User $item);

    /**
     * @param Id $id
     *
     * @return mixed
     */
    public function removeById(Id $id);

    /**
     * @return string
     */
    public function generateId();

    /**
     * @param Id $id
     *
     * @throws EntityNotFoundException
     *
     * @return User
     */
    public function byId(Id $id);

    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param $input
     *
     * @return User|null
     */
    public function findOneByScreenNameOrEmail($input);

    public function findOneByEmail($email);

    public function findOneByScreenName($screenName);
}
