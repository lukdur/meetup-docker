<?php

namespace Domain\User\Repository;

trait UserRepositoryAwareTrait
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function setUserRepository(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return UserRepositoryInterface
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * @return UserRepositoryInterface
     */
    public function userRepository()
    {
        return $this->getUserRepository();
    }
}
