<?php

namespace Domain\User\Util;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

trait PasswordEncoderAwareTrait
{
    /** @var PasswordEncoderInterface */
    private $passwordEncoder;

    /**
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function setPasswordEncoder(PasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param $password
     *
     * @return string
     */
    protected function encodePassword($password)
    {
        return $this->passwordEncoder->encodePassword(trim($password), null);
    }
}
