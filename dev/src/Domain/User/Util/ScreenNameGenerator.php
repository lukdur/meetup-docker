<?php

namespace Domain\User\Util;

class ScreenNameGenerator
{
    public static function generate($username)
    {
        return $username ? $username : 'user'.rand(100000, 999999);
    }
}
