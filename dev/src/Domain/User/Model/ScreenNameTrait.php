<?php

namespace Domain\User\Model;

use Doctrine\ORM\Mapping as ORM;
use Domain\User\Util\ScreenNameGenerator;
use JMS\Serializer\Annotation as Serializer;

trait ScreenNameTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "public_list", "private_list", "public_item", "private_item"})
     */
    private $screenName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "public_list", "private_list", "public_item", "private_item"})
     */
    private $fullName;

    /**
     * @param string $screenName
     *
     * @return self
     */
    public function setScreenName($screenName)
    {
        $this->screenName = ScreenNameGenerator::generate($screenName);

        return $this;
    }

    /**
     * @return string
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }
}
