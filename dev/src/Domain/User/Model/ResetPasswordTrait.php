<?php

namespace Domain\User\Model;

use Doctrine\ORM\Mapping as ORM;

trait ResetPasswordTrait
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $resetPasswordToken = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $resetPasswordTokenExpiration = null;

    /**
     * @return mixed
     */
    public function getResetPasswordToken()
    {
        return $this->resetPasswordToken;
    }

    /**
     * @param mixed $resetPasswordToken
     */
    public function setResetPasswordToken($resetPasswordToken)
    {
        $this->resetPasswordToken = $resetPasswordToken;
    }

    /**
     * @return mixed
     */
    public function getResetPasswordTokenExpiration()
    {
        return $this->resetPasswordTokenExpiration;
    }

    /**
     * @param mixed $resetPasswordTokenExpiration
     */
    public function setResetPasswordTokenExpiration($resetPasswordTokenExpiration)
    {
        $this->resetPasswordTokenExpiration = $resetPasswordTokenExpiration;
    }
}
