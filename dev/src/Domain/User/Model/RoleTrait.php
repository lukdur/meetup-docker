<?php

namespace Domain\User\Model;

use Doctrine\ORM\Mapping as ORM;

trait RoleTrait
{
    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;

        if (!$roles) {
            $roles = [UserInterface::ROLE_USER];
        }

        return $roles;
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function addRole($role)
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = strtoupper($role);
        }

        return $this;
    }

    /**
     * @param $role
     *
     * @return $this
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @param $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->roles, true);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole(UserInterface::ROLE_ADMIN);
    }
}
