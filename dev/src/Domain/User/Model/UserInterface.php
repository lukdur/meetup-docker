<?php

namespace Domain\User\Model;

interface UserInterface
{
    const SORTABLE_FIELDS = [
        'createdAt',
    ];

    const USER_PASSWORD_MIN_LENGTH = 6;
    const USER_PASSWORD_MAX_LENGTH = 20;

    const USER_SCREEN_NAME_MIN_LENGTH = 1;
    const USER_SCREEN_NAME_MAX_LENGTH = 20;

    const USER_FULL_NAME_MAX_LENGTH = 40;

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
}
