<?php

namespace Domain\User\Model;

use Btn\Domain\ValueObject\Id;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass="DomainBundle\Repository\UserRepository")
 * @ORM\Table(name="users",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="email_unique", columns={"email"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 *
 * @UniqueEntity(fields={"email"}, groups={"registration"})
 * @UniqueEntity(fields={"screenName"}, groups={"registration"})
 *
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom = {
 *     "id",
 *     "screen_name",
 *     "email",
 *     "created_at",
 *     "updated_at",
 * })
 */
class User implements UserInterface
{
    use RoleTrait;
    use PasswordTrait;
    use ScreenNameTrait;
    use EmailTrait;
    use DateTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=36)
     * @ORM\Id
     * ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "public_list", "private_list", "public_item", "private_item", "user_select2ajax_search"})
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "public_list", "private_list", "public_item", "private_item"})
     */
    private $type = 'user';

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public function __construct($id)
    {
        $this->id = (string) Id::create($id);

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int|string
     */
    public function getUsername()
    {
        if (!empty($this->email)) {
            return $this->email;
        }

        if (!empty($this->facebookId)) {
            return $this->facebookId;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
