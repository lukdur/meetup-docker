<?php

namespace Domain\User\Model;

use Doctrine\ORM\Mapping as ORM;

trait PasswordTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $password;

    /**
     * @param string $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
    }
}
