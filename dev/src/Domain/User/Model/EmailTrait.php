<?php

namespace Domain\User\Model;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait EmailTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"private_list", "private_item"})
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $emailConfirmed = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $confirmationToken;

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param bool $emailConfirmed
     *
     * @return self
     */
    public function setEmailConfirmed($emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    /**
     * @param string $confirmationToken
     *
     * @return self
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("text")
     * @Serializer\Groups({"user_select2ajax_search"})
     *
     * @return string
     */
    public function getEmailAsText()
    {
        $text = $this->email;
        if ($this->getScreenName()) {
            $text .= " ({$this->getScreenName()})";
        }

        return $text;
    }
}
