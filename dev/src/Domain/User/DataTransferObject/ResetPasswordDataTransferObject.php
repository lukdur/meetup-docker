<?php

namespace Domain\User\DataTransferObject;

use Assert\Assert;
use Btn\Domain\DataTransferObject\AbstractDataTransferObject;

class ResetPasswordDataTransferObject extends AbstractDataTransferObject
{
    /**
     * @var
     */
    public $password;

    /**
     *
     */
    public function validate()
    {
        parent::validate();

        Assert::that($this->password)->minLength(6);
    }
}
