<?php

namespace Domain\User\DataTransferObject;

use Domain\User\Model\User;

class UserDataTransferObjectFactory
{
    /**
     * @return UserDataTransferObject
     */
    public static function createBlank()
    {
        $dto = new UserDataTransferObject();
        $dto->isAdmin = true;

        return $dto;
    }

    /**
     * @param User $user
     *
     * @return UserDataTransferObject
     */
    public static function createFromModel(User $user)
    {
        $dto = new UserDataTransferObject($user->getId());

        $dto->screenName = $user->getScreenName();
        $dto->email = $user->getEmail();
        $dto->isAdmin = true;
        $dto->lastLogin = $user->getLastLoginAt();
        $dto->createdAt = $user->getCreatedAt();


        return $dto;
    }
}
