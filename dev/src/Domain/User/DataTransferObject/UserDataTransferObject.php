<?php

namespace Domain\User\DataTransferObject;

use Assert\Assert;
use Btn\Domain\DataTransferObject\AbstractDataTransferObject;
use Domain\User\Model\UserInterface;

class UserDataTransferObject extends AbstractDataTransferObject
{
    /** @var string */
    public $email;
    /** @var string */
    public $screenName;
    /** @var string */
    public $fullName;
    /** @var string */
    public $password;
    /** @var bool */
    public $isAdmin;
    /** @var bool */
    public $isVerified;

    public $lastLogin;

    public $createdAt;

    public function validate()
    {
        parent::validate();

        Assert::that($this->email)->nullOr()->email();
        Assert::that($this->screenName)->nullOr()->string()->minLength(UserInterface::USER_SCREEN_NAME_MIN_LENGTH)->maxLength(UserInterface::USER_SCREEN_NAME_MAX_LENGTH);
        Assert::that($this->fullName)->nullOr()->string()->minLength(UserInterface::USER_SCREEN_NAME_MIN_LENGTH)->maxLength(UserInterface::USER_SCREEN_NAME_MAX_LENGTH);
        Assert::that($this->password)->nullOr()->string()->maxLength(UserInterface::USER_FULL_NAME_MAX_LENGTH);
        Assert::that($this->isAdmin)->nullOr()->boolean();
        Assert::that($this->isVerified)->nullOr()->boolean();
    }
}
