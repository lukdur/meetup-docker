<?php

namespace Domain\User\Event;

use Btn\Domain\MessageBus\AbstractEvent;

abstract class AbstractUserEvent extends AbstractEvent
{
}
