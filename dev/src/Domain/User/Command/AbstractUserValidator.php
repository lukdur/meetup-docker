<?php

namespace Domain\User\Command;

use Btn\Domain\Exception\InvalidArgumentException;
use Domain\User\Repository\UserRepositoryAwareTrait;

abstract class AbstractUserValidator
{
    use UserRepositoryAwareTrait;

    protected function doValidate($command)
    {
        $this->validateEmail($command);
        $this->validateScreenName($command);
    }

    private function validateEmail($command)
    {
        if (is_null($command->email)) {
            return;
        }

        $user = $this->userRepository()->findOneByEmail($command->email);
        if (!$user) {
            return;
        }

        if ((string) $user->getId() === (string) $command->id()) {
            return;
        }

        throw InvalidArgumentException::createUnique('email', $command->email);
    }

    private function validateScreenName($command)
    {
        if (is_null($command->screenName)) {
            return;
        }

        $user = $this->userRepository()->findOneByScreenName($command->screenName);
        if (!$user) {
            return;
        }

        if ((string) $user->getId() === (string) $command->id()) {
            return;
        }

        throw InvalidArgumentException::createUnique('screen_name', $command->screenName, 'Username');
    }
}
