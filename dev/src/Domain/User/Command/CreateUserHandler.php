<?php

namespace Domain\User\Command;

use Domain\User\Event\UserCreated;
use Domain\User\Model\User;

class CreateUserHandler extends AbstractUserHandler
{
    public function handle(CreateUser $command)
    {
        $id = $command->id() ?: $this->userRepository()->generateId();

        $user = new User($id);

        $this->fillFromDataTransferObject($user, $command);

        $this->userRepository()->save($user);

        $this->handleEvent(new UserCreated($id));
    }
}
