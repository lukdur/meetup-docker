<?php

namespace Domain\User\Command;

class RegisterUserValidator extends AbstractUserValidator
{
    public function validate(RegisterUser $command)
    {
        $this->doValidate($command);
    }
}
