<?php

namespace Domain\User\Command;

use Domain\User\Repository\UserRepositoryAwareTrait;
use Domain\User\Util\PasswordEncoderAwareTrait;

class ChangePasswordHandler
{
    use UserRepositoryAwareTrait;
    use PasswordEncoderAwareTrait;

    public function handle(ChangePassword $command)
    {
        $user = $this->userRepository()->byId($command->id());

        $user->setPassword($this->encodePassword($command->password()));

        $this->userRepository()->save($user);
    }
}
