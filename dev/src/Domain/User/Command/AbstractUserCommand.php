<?php

namespace Domain\User\Command;

use Domain\User\DataTransferObject\UserDataTransferObject;

abstract class AbstractUserCommand extends UserDataTransferObject
{
}
