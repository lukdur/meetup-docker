<?php

namespace Domain\User\Command;

use Assert\Assert;

class CreateUser extends AbstractUserCommand
{
    public function validate()
    {
        parent::validate();

        Assert::that($this->email)->notBlank();
        Assert::that($this->password)->notBlank();
    }
}
