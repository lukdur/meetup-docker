<?php

namespace Domain\User\Command;

use Domain\User\Repository\UserRepositoryAwareTrait;

class RemoveRoleHandler
{
    use UserRepositoryAwareTrait;

    public function handle(RemoveRole $command)
    {
        $user = $this->userRepository()->byId($command->id());

        $user->removeRole($command->role());

        $this->userRepository()->save($user);
    }
}
