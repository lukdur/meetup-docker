<?php

namespace Domain\User\Command;

use Btn\Domain\MessageBus\EventBusAwareTrait;
use Domain\User\Event\UserDeleted;
use Domain\User\Repository\UserRepositoryAwareTrait;

class DeleteUserHandler
{
    use UserRepositoryAwareTrait;
    use EventBusAwareTrait;

    public function handle(DeleteUser $command)
    {
        $this->userRepository()->removeById($command->id());

        $this->handleEvent(new UserDeleted($command->id()));
    }
}
