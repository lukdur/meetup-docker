<?php

namespace Domain\User\Command;

use Btn\Domain\MessageBus\EventBusAwareTrait;
use Domain\User\Event\UserRegistered;
use Domain\User\Model\User;
use Domain\User\Repository\UserRepositoryAwareTrait;

class RegisterUserHandler extends AbstractUserHandler
{
    use EventBusAwareTrait;
    use UserRepositoryAwareTrait;

    /**
     * @param RegisterUser $command
     */
    public function handle(RegisterUser $command)
    {
        $id = $command->id() ?: $this->userRepository()->generateId();
        $user = new User($id);
        $this->fillFromDataTransferObject($user, $command);
        $this->userRepository()->save($user);
        $this->handleEvent(new UserRegistered($id));
    }
}
