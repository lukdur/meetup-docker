<?php

namespace Domain\User\Command;

use Assert\Assert;
use Btn\Domain\MessageBus\AbstractCommand;

class ChangePassword extends AbstractCommand
{
    private $password;

    /**
     * @param mixed  $id
     * @param string $password
     */
    public function __construct($id, $password)
    {
        parent::__construct($id);

        $this->password = trim((string) $password);
    }

    public function validate()
    {
        Assert::that($this->password())->notBlank();
    }

    public function password()
    {
        return $this->password;
    }
}
