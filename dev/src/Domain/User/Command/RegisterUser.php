<?php

namespace Domain\User\Command;

use Domain\User\DataTransferObject\UserDataTransferObject;
use Domain\User\Model\User;

class RegisterUser extends UserDataTransferObject
{
    public static function createFromUser(User $user)
    {
        return self::createFromModel($user);
    }

    public static function createFromModel(User $user)
    {
        $dto = new self($user->getId());

        $dto->email = $user->getEmail();
        $dto->password = $user->getPassword();
        $dto->screenName = $user->getScreenName();
        $dto->fullName = $user->getFullName();

        return $dto;
    }
}
