<?php

namespace Domain\User\Command;

use Domain\User\Event\UserUpdated;
use Domain\User\Model\User;

class UpdateUserHandler extends AbstractUserHandler
{
    public function handle(UpdateUser $command)
    {
        /** @var User $user */
        $user = $this->userRepository()->byId($command->id());

        $this->fillFromDataTransferObject($user, $command);

        $this->userRepository()->save($user);

        $this->handleEvent(new UserUpdated($command->id()));
    }
}
