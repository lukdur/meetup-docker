<?php

namespace Domain\User\Command;

use Assert\Assert;
use Btn\Domain\MessageBus\AbstractCommand;

abstract class AbstractRoleCommand extends AbstractCommand
{
    private $role;

    /**
     * @param \Btn\Domain\ValueObject\Id|null|string $id
     * @param string                             $role
     */
    public function __construct($id, $role)
    {
        parent::__construct($id);

        $this->role = $role;
    }

    public function validate()
    {
        parent::validate();

        Assert::that($this->role)->notBlank()->regex('~^[A-Za-z_]+$~');
    }

    public function role()
    {
        return strtoupper($this->role);
    }
}
