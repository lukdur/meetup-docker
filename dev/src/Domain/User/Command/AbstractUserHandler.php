<?php

namespace Domain\User\Command;

use Btn\Domain\MessageBus\EventBusAwareTrait;
use Domain\User\DataTransferObject\UserDataTransferObject;
use Domain\User\Model\User;
use Domain\User\Model\UserInterface;
use Domain\User\Repository\UserRepositoryAwareTrait;
use Domain\User\Util\PasswordEncoderAwareTrait;

class AbstractUserHandler
{
    use UserRepositoryAwareTrait;
    use EventBusAwareTrait;
    use PasswordEncoderAwareTrait;

    /**
     * @param User                   $user
     * @param UserDataTransferObject $command
     */
    protected function fillFromDataTransferObject(User $user, UserDataTransferObject $command)
    {
        if (!is_null($command->email)) {
            $user->setEmail($command->email);
        }

        if (!is_null($command->screenName)) {
            $user->setScreenName($command->screenName);
        }

        if (!is_null($command->password)) {
            $user->setPassword($this->encodePassword($command->password));
        }

        $this->fillRoleFromDataTransferObject($user, $command);
    }

    /**
     * @param User                   $user
     * @param UserDataTransferObject $command
     */
    private function fillRoleFromDataTransferObject(User $user, UserDataTransferObject $command)
    {
        if (!is_null($command->isAdmin)) {
            if ($command->isAdmin) {
                $user->addRole(UserInterface::ROLE_ADMIN);
            } else {
                $user->removeRole(UserInterface::ROLE_ADMIN);
            }
        }
    }
}
