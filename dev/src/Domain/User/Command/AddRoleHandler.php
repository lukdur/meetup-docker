<?php

namespace Domain\User\Command;

use Domain\User\Repository\UserRepositoryAwareTrait;

class AddRoleHandler
{
    use UserRepositoryAwareTrait;

    public function handle(AddRole $command)
    {
        $user = $this->userRepository()->byId($command->id());

        $user->addRole($command->role());

        $this->userRepository()->save($user);
    }
}
