<?php

namespace DomainBundle\Util;

use Domain\User\Model\User;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Controller utils.
 *
 * @DI\Service("app.controller_utils")
 *
 * @link http://www.whitewashing.de/2013/06/27/extending_symfony2__controller_utilities.html
 */
class ControllerUtils
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @DI\InjectParams()
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Generates a URL.
     *
     * @param string $route
     * @param mixed $parameters
     * @param int $referenceType
     *
     * @return string
     */
    public function generateUrl($route, $parameters = [], $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Generates a URL.
     *
     * @param string $route
     * @param mixed $parameters
     *
     * @return string
     */
    public function generateAbsoluteUrl($route, $parameters = [])
    {
        return $this->generateUrl($route, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * Render view.
     *
     * @param string $view
     * @param array $parameters
     * @param Response $response
     *
     * @return Response
     */
    public function render($view, array $parameters = [], Response $response = null)
    {
        return $this->container->get('templating')->renderResponse($view, $parameters, $response);
    }

    public function renderHtml($view, $parameters, $status = 200)
    {
        return $this->render($view, $parameters, new Response('', $status, ['Content-Type' => 'text/html']));
    }

    /**
     * Create a Form instance.
     *
     * @param string $type
     * @param mixed $data
     * @param array $options
     *
     * @return Form
     */
    public function createForm($type, $data = null, array $options = [])
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }

    /**
     * Get the authenticated user.
     *
     * @return User|object|null
     *
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {
        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }

    /**
     * Is the visitor a guest (true) or logged in customer (false).
     *
     * @return bool
     */
    public function isUser()
    {
        return $this->getUser() instanceof User;
    }

    /**
     * @param string|null $route
     * @param array $params
     * @param null $data
     *
     * @return View|Response
     */
    public function createCreatedView($route = null, $params = [], $data = null)
    {
        if ($route) {
            return View::create(
                $data,
                Response::HTTP_CREATED,
                [
                    'Location' => $this->generateAbsoluteUrl($route, $params),
                ]
            );
        }

        return $this->createNoContentView();
    }

    /**
     * @param array $headers
     *
     * @return View
     */
    public function createNoContentView($headers = [])
    {
        return View::create(null, Response::HTTP_NO_CONTENT, $headers);
    }

    /**
     * @param FormInterface $form
     *
     * @return JsonResponse
     */
    public function createErrorResponseFromForm(FormInterface $form)
    {
        $errors = $this->extractFormErrors($form);

        return $this->jsonErrorResponse($errors);
    }

    /**
     * @param $data
     *
     * @return View
     */
    public function createSuccessResponseView($data)
    {
        return View::create($data, Response::HTTP_OK);
    }

    public function createUnauthorizedResponseView($data = null, $headers = [])
    {
        return View::create($data, Response::HTTP_UNAUTHORIZED, $headers);
    }

    /**
     * @param $errors
     * @param int $code
     *
     * @return JsonResponse
     */
    public function jsonErrorResponse($errors, $code = Response::HTTP_BAD_REQUEST)
    {
        return new JsonResponse([
            'errors' => $errors,
        ], $code);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function extractFormErrors(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors(true, true) as $error) {
            if ($error instanceof FormError) {
                $errors[] = [
                    'parameter' => $error->getOrigin()->getName(),
                    'error' => $error->getMessage(),
                ];
            }
        }

        return $errors;
    }

    public function render404Page($msg = 'Page not Found')
    {
        return $this->renderHtml('::404.html.twig', ['msg' => $msg], 404);
    }

    public function redirectToUrl($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
}
