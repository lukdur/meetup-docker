<?php

namespace DomainBundle\Util;

use Btn\Domain\MessageBus\CommandBusAwareTrait;
use Domain\User\Command\AddRole;
use Domain\User\Command\ChangePassword;
use Domain\User\Command\CreateUser;
use Domain\User\Command\RemoveRole;
use Domain\User\Repository\UserRepositoryAwareTrait;
use FOS\UserBundle\Util\UserManipulator as BaseManipulator;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service()
 */
class UserManipulator extends BaseManipulator
{
    use CommandBusAwareTrait;
    use UserRepositoryAwareTrait;

    public function create($username, $password, $email, $active, $superadmin)
    {
        $command = new CreateUser();
        $command->screenName = $username;
        $command->password = $password;
        $command->email = $email;

        $this->handleCommand($command);
    }

    public function activate($username)
    {
        throw new \Exception('Implement me');
    }

    public function deactivate($username)
    {
        throw new \Exception('Implement me');
    }

    public function changePassword($username, $password)
    {
        $user = $this->findUserByUsernameOrThrowException($username);
        $this->handleCommand(new ChangePassword($user, $password));
    }

    public function addRole($username, $role)
    {
        $user = $this->findUserByUsernameOrThrowException($username);
        if ($user->hasRole($role)) {
            return false;
        }

        $this->handleCommand(new AddRole($user, $role));

        return true;
    }

    public function removeRole($username, $role)
    {
        $user = $this->findUserByUsernameOrThrowException($username);
        if (!$user->hasRole($role)) {
            return false;
        }

        $this->handleCommand(new RemoveRole($user, $role));

        return true;
    }

    private function findUserByUsernameOrThrowException($username)
    {
        $user = $this->userRepository()->findOneByScreenNameOrEmail($username);

        if (!$user) {
            throw new \InvalidArgumentException(sprintf('User identified by "%s" username does not exist.', $username));
        }

        return $user;
    }
}
