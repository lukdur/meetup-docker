<?php

namespace DomainBundle\Util;

trait ControllerUtilsAwareTrait
{
    /** @var ControllerUtils */
    private $controllerUtils;

    public function setControllerUtils(ControllerUtils $controllerUtils)
    {
        $this->controllerUtils = $controllerUtils;
    }

    public function controllerUtils()
    {
        return $this->controllerUtils;
    }
}
