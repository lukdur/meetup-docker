<?php

namespace DomainBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class UserManipulatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('fos_user.util.user_manipulator')) {
            return;
        }

        $definition = $container->getDefinition('fos_user.util.user_manipulator');
        $definition->setClass('DomainBundle\\Util\\UserManipulator');
    }
}
