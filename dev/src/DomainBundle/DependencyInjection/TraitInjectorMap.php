<?php

namespace DomainBundle\DependencyInjection;

use Domain\User\Repository\UserRepositoryAwareTrait;
use Domain\User\Util\PasswordEncoderAwareTrait;
use DomainBundle\Util\ControllerUtilsAwareTrait;

class TraitInjectorMap
{
    public static function getMap()
    {
        return [
            UserRepositoryAwareTrait::class => ['setUserRepository', 'domain.repository.user'],
            PasswordEncoderAwareTrait::class => ['setPasswordEncoder', 'domain.security.password_encoder'],
            ControllerUtilsAwareTrait::class => ['setControllerUtils', 'app.controller_utils'],
        ];
    }
}
