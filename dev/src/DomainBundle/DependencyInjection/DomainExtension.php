<?php

namespace DomainBundle\DependencyInjection;

use Btn\DomainBundle\DependencyInjection\DomainExtension as BaseExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\Kernel;

class DomainExtension extends BaseExtension
{
    /**
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        parent::prepend($container);
        $container->setParameter('app.src_dir', realpath(__DIR__.'/../../../src'));
//         get config loader
        $loader = $this->getConfigLoader($container);
        $loader->tryLoadForExtension('security');
        $loader->tryLoadForExtension('doctrine');
        $loader->tryLoadForExtension('doctrine_orm_bridge');
        $loader->tryLoadForExtension('doctrine_migrations');

        $env = $container->getParameter('kernel.environment');

        if ('test' === $env) {
            $loader->tryLoadForExtension('doctrine', 'doctrine_test');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        parent::loadInternal($mergedConfig, $container);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('aliases.yml');
        $loader->load('listener.yml');
        $loader->load('repository.yml');
        $loader->load('services.yml');
        if (Kernel::VERSION_ID >= 30200) {
            $loader->load('services_autowire.yml');
        }
        if ($mergedConfig['persistence']['orm']['enabled']) {
            $container->setParameter($this->getAlias().'.persistence.orm.enabled', true);
        }
    }
}
