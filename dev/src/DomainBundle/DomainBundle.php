<?php

namespace DomainBundle;

use DomainBundle\DependencyInjection\TraitInjectorMap;
use DomainBundle\DependencyInjection\DomainExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Btn\DomainBundle\DependencyInjection\Compiler\TraitInjectorCompilerPass;
use DomainBundle\DependencyInjection\Compiler\UserManipulatorCompilerPass;

class DomainBundle extends \Btn\DomainBundle\DomainBundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new UserManipulatorCompilerPass());

        parent::build($container);
        $container->addCompilerPass(new TraitInjectorCompilerPass(TraitInjectorMap::getMap()));
    }

    public function getContainerExtension()
    {
        if ($this->extension === null) {
            $this->extension = new DomainExtension();
        }

        return $this->extension;
    }
}
