<?php

namespace DomainBundle\Repository;

use Assert\Assert;
use Domain\User\Model\User;
use Doctrine\ORM\NoResultException;
use Domain\User\Repository\UserRepositoryInterface;
use Btn\Domain\ValueObject\Id;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Btn\DomainBundle\Repository\AbstractEntityRepository;

/**
 * @method User find($id, $lockMode = null, $lockVersion = null)
 */
class UserRepository extends AbstractEntityRepository implements
    UserProviderInterface,
    UserRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function save(User $user)
    {
        $this->doSave($user);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(User $user)
    {
        $this->doRemove($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.email = :username')
            ->orWhere('u.screenName = :username')
            ->setParameter('username', $username)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $ex = new UsernameNotFoundException(sprintf('There is no user with name "%s".', $username));
            $ex->setUsername($username);
            throw $ex;
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByScreenNameOrEmail($input)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where('u.email = :input');
        $qb->orWhere('u.screenName = :input');
        $qb->setParameter('input', $input);

        $q = $qb->getQuery();
        $q->setMaxResults(2);

        $result = $q->getResult();

        if (count($result) > 1) {
            throw new \Exception('To many user matched');
        }

        return $result ? $result[0] : false;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByEmail($email)
    {
        return $this->findOneBy(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByScreenName($screenName)
    {
        return $this->findOneBy(['screenName' => $screenName]);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());
    }

    public function getAll($user = null)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->orderBy('u.id', 'ASC');

        if ($user instanceof User) {
            $qb
                ->where('u.id != :userId')
                ->setParameter('userId', $user->getId());
        }

        $q = $qb->getQuery();

        $users = $q->getResult();

        return $users;
    }

    public function byResetPasswordToken($token)
    {
        $query = $this->createQueryBuilder('u')
                      ->where('u.resetPasswordToken = :token')
                      ->andWhere('u.resetPasswordTokenExpiration < :now')
                      ->setParameter('token', $token)
                      ->setParameter('now', new \DateTime())
                      ->getQuery();

        return $query->getSingleResult();
    }

    protected function validateEntityClass($entity)
    {
        Assert::that($entity)->isInstanceOf(User::class);
    }
}
