<?php

namespace DomainBundle;

use Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle;
use Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle;
use Symfony\Bundle\DebugBundle\DebugBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;

abstract class AbstractKernel extends \Btn\DomainBundle\AbstractKernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = array_merge([
            new DomainBundle(),
        ], parent::registerBundles());

        $bundles[] = new DoctrineMigrationsBundle();

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new DebugBundle();
            $bundles[] = new WebProfilerBundle();
            $bundles[] = new SensioGeneratorBundle();
        }

        return $bundles;
    }
}
