<?php

namespace ApiBundle\Form\Type;

use Domain\User\DataTransferObject\UserDataTransferObject;
use Domain\User\Model\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @DI\FormType()
 */
class UserRegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'email',
            EmailType::class,
            [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                ],
            ]
        );

        $builder->add(
            'password',
            PasswordType::class,
            [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => UserInterface::USER_PASSWORD_MIN_LENGTH,
                        'max' => UserInterface::USER_PASSWORD_MAX_LENGTH,
                    ]),
                ],
            ]
        );

        $builder->add(
            'screen_name',
            TextType::class,
            [
                'property_path' => 'screenName',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => UserInterface::USER_SCREEN_NAME_MIN_LENGTH,
                        'max' => UserInterface::USER_SCREEN_NAME_MAX_LENGTH,
                    ]),
                ],
            ]
        );

        $builder->add(
            'full_name',
            TextType::class,
            [
                'property_path' => 'fullName',
                'constraints' => [
                    new Assert\Length([
                        'max' => UserInterface::USER_FULL_NAME_MAX_LENGTH,
                    ]),
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UserDataTransferObject::class,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return null;
    }
}
