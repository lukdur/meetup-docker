<?php

namespace ApiBundle\Controller\Auth;

use DomainBundle\Util\ControllerUtilsAwareTrait;
use Assert\AssertionFailedException;
use Btn\Domain\MessageBus\CommandBusAwareTrait;
use Domain\User\Repository\UserRepositoryAwareTrait;
use ApiBundle\Form\Type\UserRegistrationType;
use Domain\User\Command\RegisterUser;
use Btn\DomainBundle\Util\FormUtil;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @DI\Service()
 */
class Register
{
    use UserRepositoryAwareTrait;
    use CommandBusAwareTrait;
    use ControllerUtilsAwareTrait;

    /** @var JWTManagerInterface */
    private $jwtManager;

    /**
     * @DI\InjectParams()
     *
     * @param JWTManagerInterface $jwtManager
     */
    public function __construct(
        JWTManagerInterface $jwtManager
    ) {
        $this->jwtManager = $jwtManager;
    }

    /**
     * Registration.
     *
     * @Rest\Post("/auth/register", name="auth_register")
     *
     * @ApiDoc(
     *  section="Auth",
     *  resource=false,
     *  description="Register new user",
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="email"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="password"},
     *      {"name"="screen_name", "dataType"="string", "required"=true, "description"="screen_name"},
     *      {"name"="full_name", "dataType"="string", "required"=false, "description"="full_name"},
     *  }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse|Response|View
     */
    public function __invoke(Request $request)
    {
        $form = $this->controllerUtils()->createForm(UserRegistrationType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $id = $this->userRepository()->generateId();

                $command = new RegisterUser($id);
                $command->fillFrom($form->getData());
                $this->handleCommand($command);

                $user = $this->userRepository()->byId($command->id());
                $token = $this->jwtManager->create($user);

                return $this->controllerUtils()->createCreatedView('user_profile', [
                    'user_id' => $user->getId(),
                ], [
                    'token' => $token,
                    'user' => $user,
                ]);
            } catch (AssertionFailedException $e) {
                FormUtil::passValidationErrorsToForm($e, $form);
            }
        }

        return $this->controllerUtils()->createErrorResponseFromForm($form);
    }
}
