<?php

namespace ApiBundle\Controller\Auth;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

/**
 *
 */
class Login
{
    /**
     * Auth login.
     *
     * @Rest\GET("/auth/login", name="auth_login_info")
     * @Rest\Post("/auth/login", name="auth_login_doc")
     *
     * @ApiDoc(
     *  section="Auth",
     *  resource=false,
     *  description="Allow users to login",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="username"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="password"}
     *  }
     * )
     */
    public function __invoke()
    {
        return View::create(
            [
                'code' => 0,
                'message' => 'You are not logged in',
                'url' => 'POST /auth/login',
                'parameters' => [
                    'username' => 'username or email',
                    'password' => 'your secret password',
                ],
            ],
            Response::HTTP_OK
        );
    }
}
