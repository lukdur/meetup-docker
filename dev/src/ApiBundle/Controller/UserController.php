<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service()
 */
class UserController extends Controller
{
    /**
     * @Rest\Get("/user", name="some_user")
     */
    public function indexAction(Request $request)
    {
        return View::create(
            [
                'xyz' => 'abc',
            ],
            Response::HTTP_OK
        );
    }
}
