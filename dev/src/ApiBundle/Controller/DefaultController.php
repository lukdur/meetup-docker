<?php

namespace ApiBundle\Controller;

use Btn\Domain\MessageBus\CommandBusAwareTrait;
use Domain\User\Repository\UserRepositoryAwareTrait;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service()
 */
class DefaultController extends Controller
{
    use CommandBusAwareTrait;
    use UserRepositoryAwareTrait;
    /**
     * @Rest\Get("/", name="home")
     */
    public function indexAction(Request $request)
    {
        return View::create(
            [
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/is_alive", name="is_alive")
     */
    public function isAliveAction()
    {
        if (true) {
        }

        return View::create(
            [
                'status' => 'OK',
            ],
            Response::HTTP_OK
        );
    }
}
