<?php

namespace ApiBundle\Controller\User;

use Domain\User\Repository\UserRepositoryAwareTrait;
use DomainBundle\Util\ControllerUtilsAwareTrait;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Btn\DomainBundle\Util\Uuid;

/**
 * @DI\Service()
 */
class Profile
{
    use ControllerUtilsAwareTrait;
    use UserRepositoryAwareTrait;

    /**
     * User profile.
     *
     * @Rest\Get("/users/{user_id}", name="user_profile", requirements={"user_id": Uuid::UUID1_ROUTE_PATTERN})
     *
     * @ApiDoc(
     *  section="Content",
     *  resource=false,
     *  description="User profile"
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function __invoke(Request $request)
    {
        $userId = $request->attributes->get('user_id');
        $user = $this->userRepository()->find($userId);

        return $this->controllerUtils()->createSuccessResponseView([
            'user' => $user,
        ]);
    }
}
