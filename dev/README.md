Bitnoise Rest Api Boilerplate

Api Documentation is on `/doc` endpoint

## Installation
```
git clone git@bitbucket.org:btndev/symfony-api-boilerplate.git your-package-name
cd your-package-name

# if ask sudo then input the password
# Set `private_key_pass_phrase` and use the same password, when script ask about JWT token password 
# Set valid mysql credentials, because script will try create database
# Wizard will ask you, will you want to install database on Docker or in your system. 
# If you choose Docker, then leave default database credentials!
composer install
# replace git repository

Check is working on `/is_alive` endpoint
```

## Git repository replacement
```
cd `your-package-name` 
```

#### Automatically:
```
/bin/bash dev.sh replace_git_repository ${repository-url}
```
#### or manually:
```
rm -rf .git
git init
git remote add origin ${repository-url}
/bin/bash dev.sh install_pre_commit
```

## Sql migration
```
# create migration
bin/console doctrine:migrations:diff or /bin/bash dev.sh create_migration
# execute migration
bin/console doctrine:migrations:migrate or /bin/bash dev.sh migrate 
```

## Creating new user
```
bin/console fos:user:create # create user
bin/console fos:user:promote # input user email as name and role: ROLE_ADMIN 
```

## Dev.sh 
```
always remember to EXECUTE that script from APPLICATION ROOT DIRECTORY! (where dev.sh script is default placed)
```
* set_permissions - run it when composer install
* create_database - run it when composer install
* create_migration
* migrate
* generate_jwt_tokens - remove `var/jwt` directory first 
* replace_git_repository ${repository-url}
* lint - validate code
* fix - fix code checked by linter
* phpqa


## Linter

Before commit linter checks is everything ok. If not, it will disable commit.
1) You can fix all linter issues with `/bin/bash dev.sh fix`
2) You can lint code manually with `/bin/bash dev.sh lint`
3) You can skip linter validation in commit with `git commit --no-verify`

## PHPQA
You can use all options from `phpqa` package https://github.com/jakzal/phpqa

Run: `/bin/bash dev.sh phpqa {...arguments}`

## Using Docker

Application could use mysql database in Docker.
When you start your development, you have to run docker services. 
To do that run:
* `/bin/bash dev.sh start`
* `/bin/bash dev.sh stop`