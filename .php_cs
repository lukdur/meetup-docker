<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__ . '/src'
    ])
;

return PhpCsFixer\Config::create()
    ->setFinder($finder)
    ->setRules(
        [
            '@PSR2' => true,
            'blank_line_after_opening_tag' => true,
            'concat_space' => true,
            'no_multiline_whitespace_around_double_arrow' => false,
            'no_empty_statement' => true,
            'include' => true,
            // 'join_function' => true, // not in 1.1 and 5.4 ?
            'trailing_comma_in_multiline_array' => true,
            'no_leading_namespace_whitespace' => true,
            'new_with_braces' => true,
            'no_blank_lines_after_class_opening' => true,  // not in 1.1 ?
            'object_operator_without_whitespace' => true,
            'binary_operator_spaces' => [
                'align_double_arrow' => false,
                'align_equals' => false
            ],
            'phpdoc_indent' => true,
            'phpdoc_inline_tag' => true,
            'phpdoc_no_access' => true,
            'phpdoc_no_package' => true,
            'phpdoc_order' => true,
            'phpdoc_scalar' => true,
            'phpdoc_separation' => true,
            'phpdoc_to_comment' => true,
            'phpdoc_trim' => true,
            'phpdoc_types' => true,
            'phpdoc_var_without_name' => true,
            'no_leading_import_slash' => true,
            'blank_line_before_return' => true,
            'self_accessor' => true,
            'no_trailing_comma_in_singleline_array' => true,
            'single_blank_line_before_namespace' => true,
            'single_line_after_imports' => false,
            'single_quote' => true,
            'no_multiline_whitespace_before_semicolons' => true,
            'cast_spaces' => true,
            'standardize_not_equals' => true,
            'ternary_operator_spaces' => true,
            'array_syntax' => ['syntax' => 'short'],
            'trim_array_spaces' => true,
            'unary_operator_spaces' => true,
            'no_unused_imports' => true,
            'no_whitespace_in_blank_line' => true,
        ]
    )
;
