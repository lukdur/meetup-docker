#!/usr/bin/env bash
command=${1}
 say() {
     echo "$@" | sed \
             -e "s/\(\(@\(red\|green\|yellow\|blue\|magenta\|cyan\|white\|reset\|b\|u\)\)\+\)[[]\{2\}\(.*\)[]]\{2\}/\1\4@reset/      g" \
             -e "s/@red/$(tput setaf 1)/g"   \
             -e "s/@green/$(tput setaf 2)/g" \
             -e "s/@yellow/$(tput setaf 3)/g"        \
             -e "s/@blue/$(tput setaf 4)/g"  \
             -e "s/@magenta/$(tput setaf 5)/g"       \
             -e "s/@cyan/$(tput setaf 6)/g"  \
             -e "s/@white/$(tput setaf 7)/g" \
             -e "s/@reset/$(tput sgr0)/g"    \
             -e "s/@b/$(tput bold)/g"        \
             -e "s/@u/$(tput sgr 0 1)/g"
  }

  msg() {
    say @b@red[[${@}]]
  }

function mailcatcher {
    if [[ ${1} == 'start' ]];then
        msg "docker run --net host --name Mailcatcher -d -p 1080:1080 -p 1025:1025 schickling/mailcatcher"
        docker run --net host --name Mailcatcher -d -p 1080:1080 -p 1025:1025 schickling/mailcatcher
    else
        docker kill Mailcatcher
    fi
}

function phpqa {
    msg "docker run -i --rm -v $(pwd):/project -w /project jakzal/phpqa:alpine ${@}"
    docker run -i --rm -v $(pwd):/project -w /project jakzal/phpqa:alpine ${@}
}

function phpCsFixer {
     phpqa php-cs-fixer ${1}
}

if [[ ${command} == "hello" ]];then
    msg "docker run hello-world"
    docker run hello-world
fi

if [[ ${command} == "mailcatcher" ]];then
    mailcatcher ${2}
fi

if [[ ${command} == 'lint' ]];then
    phpCsFixer 'fix --dry-run --diff'
fi

if [[ ${command} == 'fix' ]];then
    phpCsFixer 'fix'
fi

if [[ ${command} == 'elasticsearch' ]];then
    msg 'docker build --no-cache -t "elasticsearch_image" ./docker/elasticsearch'
    msg "docker run -d -p 9202:9200 --name Elasticsearch elasticsearch_image"
    docker build --no-cache -t "elasticsearch_example" ./docker/elasticsearch
    docker run -d -p 9202:9200 --name Elasticsearch elasticsearch_example
fi

if [[ ${command} == 'elastichead' ]];then
    msg "docker run -d -p 9100:9100 jeanberu/elasticsearch-head-standalone"
    docker run -d -p 9100:9100 jeanberu/elasticsearch-head-standalone
fi

if [[ ${command} == 'install_dev' ]];then
    /bin/bash ./dev/docker/compose.sh build
    /bin/bash ./dev/docker/compose.sh up -d
fi
if [[ ${command} == 'start_dev' ]];then
    /bin/bash ./dev/docker/compose.sh up -d
fi
if [[ ${command} == 'stop_dev' ]];then
    /bin/bash ./dev/docker/compose.sh down
fi